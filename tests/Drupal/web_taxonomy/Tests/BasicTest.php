<?php
/*
 * @file
 * Tests for WebTaxonomy.
 */

namespace Drupal\web_taxonomy\Tests;

use Drupal\Tests\UnitTestCase;
use Drupal\web_taxonomy\WebTaxonomy;

/**
 * @group WebTaxonomy
 */
class BasicTest extends UnitTestCase {

  /* @var DummyWebTaxonomy */
  private $web_taxonomy;
  private $web_taxonomy_name = 'dummy_projects';

  public static function getInfo() {
    return array(
      'name' => 'Lookup and storing web taxonomy terms',
      'description' => 'Verify creating example nodes, accessing terms and attatching them to example nodes.',
      'group' => 'WebTaxonomy',
    );
  }

  public function setUp() {
    $this->web_taxonomy = new DummyWebTaxonomy($this->web_taxonomy_name);
  }

  public function testDummy() {
    $this->assertEquals($this->web_taxonomy_name, $this->web_taxonomy->getName());

    // cannot be test in a sensible way before function is refactored
    // to the use of some subclass of Drupal\taxonomy\Entity\Term
    //$this->web_taxonomy->saveTerm($term);
  }
}

class DummyWebTaxonomy extends WebTaxonomy {

  function fetchTerm($term) {
    // TODO: Implement fetchTerm() method.
    return (array) $term;
  }

  function autocomplete($string = '') {
    return $this->fetchTermInfo(null, null);
  }

  function fetchTermInfo($url, $params) {
    $term_info = array();

    # NO external web service call
    $data = get_test_data();

    $projects = (object) json_decode($data);
    foreach ($projects as $project) {
      $term_info[$project->title] = array(
        'name' => $project->title,
        'web_tid' => $project->uri,
      );
    }
    return $term_info;
  }
}

/**
 * Mock test environment.
 */

const LANGUAGE_NONE = 'und';

function taxonomy_term_save($term) {
  print_r($term);
}
/**
 * Function for testing when there is no Internet connection.
 */
function get_test_data() {
  return '[
    {
        "title": "apachesolr_attachments",
        "uri": "http://git.drupal.org/project/apachesolr_attachments.git"
    },
    {
        "title": "apachesolr_autocomplete",
        "uri": "http://git.drupal.org/project/apachesolr_autocomplete.git"
    },
    {
        "title": "apachesolr_multilingual",
        "uri": "http://git.drupal.org/project/apachesolr_multilingual.git"
    },
    {
        "title": "apachesolr_og",
        "uri": "http://git.drupal.org/project/apachesolr_og.git"
    },
    {
        "title": "apachesolr_page",
        "uri": "http://git.drupal.org/project/apachesolr_page.git"
    },
    {
        "title": "apachesolr",
        "uri": "http://git.drupal.org/project/apachesolr.git"
    },
    {
        "title": "apachesolr_stats",
        "uri": "http://git.drupal.org/project/apachesolr_stats.git"
    },
    {
        "title": "apachesolr_views",
        "uri": "http://git.drupal.org/project/apachesolr_views.git"
    }
]';
}

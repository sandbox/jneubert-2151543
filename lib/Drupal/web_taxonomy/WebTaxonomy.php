<?php

/**
 * @file
 * Contains Drupal\web_taxonomy\WebTaxonomy.
 */

namespace Drupal\web_taxonomy;

use Drupal\Core\Language\Language;

/**
 * A common class for Web Taxonomies.
 *
 * Required - Extend the class with an autocomplete implementation.
 */
abstract class WebTaxonomy {
  // The name of the external taxonomy, as declared in hook_web_taxonomy.
  protected $wt_name;

  /**
   * Constructs a WebTaxonomy object.
   */
  function __construct($wt_name) {
    $this->wt_name = $wt_name;
  }

  /**
   * Fetch this term from the external vocabulary.
   * 
   * @param $term
   *   The loaded taxonomy term.
   * 
   * @return
   *   An array containing the following term info:
   *     - name: The name of the term.
   *     - web_tid: The URL that acts as the universal identifier for this term.
   *     - parent_web_tids: (optional) An array of universal identifiers for
   *       parent terms. There can be multiple parents, but they must all be
   *       from the same vocabulary.
   */
  abstract function fetchTerm($term);

  /**
   * Autocompletes term against external vocabulary based on user-input.
   *
   * @param $string
   *   The letters that the user has already typed.
   *
   * @return
   *   Implementations must return an array of matches. Each match should be
   *   an array that is keyed by the term name and contains the term info
   *   as described in fetchTerm().
   */
  abstract function autocomplete($string = '');

  /**
   * Saves the term in the taxonomy.
   *
   */
  public function saveTerm($term) {
    $ext_term = $this->fetchTerm($term);

    if (!empty($ext_term['parent'])) {
      if ($parent = web_taxonomy_term_load($ext_term['parent'])) {
        
      }
      else {
        $item = array(
          'vid' => $term->vid,
          'name' => $term->name,
        );
        $item['web_tid'][Language::LANGCODE_NOT_SPECIFIED][0]['value'] = $ext_term['parent'];
        $parent = (object) $item;
        $this->saveTerm($parent);
      }
    }
    $term->name = isset($ext_term['name']) ? $ext_term['name'] : $term->name;
    print_r($term);
    $term->web_tid[Language::LANGCODE_NOT_SPECIFIED][0]['value'] = isset($ext_term['web_tid'])
        ? $ext_term['web_tid']
        : $term->web_tid[Language::LANGCODE_NOT_SPECIFIED][0]['value'];
    if (isset($parent)) {
      $term->parent = $parent->tid;
    }
    taxonomy_term_save($term);
  }

  /**
   * Update all terms of a taxonomy.
   *
   */
  public function updateVocabulary() {
    $query = db_select('taxonomy_term_data', 'data');
    $query->join('web_taxonomy_vocabulary', 'voc', 'data.vid=voc.vid');
    $query->join('field_data_web_tid', 'web_tid', 'data.tid=web_tid.entity_id');
    $query->condition('voc.name', $this->wt_name, '=');
    $query->fields('data', array('tid'));
    $tids = $query->execute()->fetchCol();
    $this->terms = taxonomy_term_load_multiple($tids);

    foreach ($this->terms as $term) {
      $operations[] = array(
        '_web_taxonomy_update_term_batch_process',
        array(
          $term,
          $this,
        ),
      );
    }
    $batch = array(
      'title' => t('Updating Web Taxonomy vocabulary'),
      'init_message' => t('Preparing to update vocabulary'),
      'operations' => $operations,
      'finished' => '_web_taxonomy_update_vocabulary_batch_finished',
      'file' => drupal_get_path('module', 'web_taxonomy') . '/web_taxonomy.admin.inc',
    );
    batch_set($batch);
    batch_process('');
  }

  /**
   * Caches information about the terms listed in the autocomplete dropdown.
   * This is cached in the database because the dropdown uses AJAX and thus the
   * match info array isn't accessible when we need it for form submission.
   *
   * @param $match_info
   *   An array of term information as returned by self::autocomplete().
   */
  public function cacheMatchInfo($match_info) {
    global $user;
    $cache = cache_get($user->uid, 'cache_web_taxonomy');
    $old_match_info = $cache->data;

    if (!empty($old_match_info)){
      $match_info = array_merge($old_match_info, $match_info);
    }
    cache_set($user->uid, $match_info, 'cache_web_taxonomy', CACHE_TEMPORARY);
  }

  /**
   * Get the name of the taxonomy.
   */
  public function getName() {
    return $this->wt_name;
  }
}

